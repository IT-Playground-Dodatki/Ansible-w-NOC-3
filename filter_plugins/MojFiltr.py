# Make coding more python3-ish
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

class FilterModule(object):

    def filters(self):
        return {
            'numer_seryjny': self.odczytaj_numer_seryjny
        }

    def odczytaj_numer_seryjny(self, data):
        if "output" in data:
            return data['output'][0]['rpc-reply']['chassis-inventory']['chassis']['serial-number']
        else:
            return data['stdout'][0]['chassis-inventory'][0]['chassis'][0]['serial-number'][0]['data']
